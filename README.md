## **Laravel + docker**
php + mysql + phpmyadmin  
## 內容
- route建構  
- 會員系統 權限檢查  
- AJAX 、API串接應用   
- 抽component應用(modal 共用元件)  
- CRUD應用(POST redirect，AJAX return、RESTful API)  

## Start Command

<p>前置作業</P>
1.建立vendor

>  切至app底下執行  
>  composer install || composer update

2.建立.env環境
- 複製.env.example進行編輯

3.啟動方式
- <p>3.1 啟動 laravel server : php artisan serve</p>
>  指定位置、port : php artisan serve --host=xxx.xxx.xxx.xxx
- <p>3.2 啟動vite(會員) : npm run dev(暫時不需要)</p>
- <p>3.3 預設位置</p>
>  app:8000  
>  mysql:3306  
>  phpmyadmin:8080(帳號root無密碼)
- <p>Docker 執行
>  1.containers包含 web(app) 、mysql、phpmyadmin  
>  2.如果執行網頁出現資料庫出問題，請檢查.env的host設定  
>  3.於GCP AWS 佈署須注意mysql版本問題(建議8以上) 


## 資料夾結構
<p>MVC</p>

- Model：放在 app/Models 目錄下。
>  建立model  
>  php artisan make:model {model name}  
>  php artisan make:model {model name} --migration

- View：放在 resources/views 目錄下，通常會以資源名稱為子目錄名稱，例如 resources/views/users。

- Controller：放在 app/Http/Controllers 目錄下，通常會以資源名稱為子目錄名稱，例如 app/Http/Controllers/UsersController.php。


<p>Other</p>
- routes：放置所有應用程式的路由檔案，可以定義 HTTP 路由、Web 路由、API 路由、控制器等等。 

> 網頁route : routes/web.php

- database：放置資料庫相關的檔案，例如資料庫遷移檔案、資料填充檔案、資料庫種子檔案等等。

- config：放置應用程式的設定檔案，例如資料庫連線設定、郵件寄送設定、認證設定等等。

- public：放置公開存取的靜態檔案，例如圖片、CSS 樣式、JavaScript 程式碼等等。

- storage 資料夾、vendor 資料夾、.env 檔案等等，其作用可以參考官方文件。

## 一些其他
- 加入navbar，檔案位置resources/view/layput/navbar.blade.php
使用方式 @extends('layouts.navbar')

- blade語法  
> @extends('layouts.app') => 套用母版  
> @section('content') => 填充 @endsection(關閉@section，一定要有)  
> @yield('content') => 套用內頁  
