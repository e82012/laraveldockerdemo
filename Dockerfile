FROM php:8.1.0

WORKDIR /app

# 安裝必要套件
# RUN apt-get update && apt-get install -y git zip unzip libzip-dev

# 安裝 Composer
# COPY --from=composer /usr/bin/composer /usr/bin/composer

# 複製 Laravel 專案
COPY . .

# 安裝專案相依套件
# RUN docker-php-ext-install pdo_mysql


# CMD ["php", "artisan", "serve"]
CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
# 
