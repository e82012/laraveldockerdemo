<!DOCTYPE html>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs@4.4.0/dist/tf.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-vis@1.5.1/dist/tfjs-vis.umd.min.js"></script>


<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @section('title', 'TestPage')
    @extends('layouts.navbar')
    @section('content')
        <p></p>
        {{$testValue}}
        <br>
        <button class="btn btn-outline-primary" onclick="tensorFlowVis()">chart</button>
        <br>
        <button class="btn btn-outline-primary" onclick="tensorFlowShow()">data</button>
        <br>
        <button class="btn btn-outline-primary" onclick="tensorFlowChartShow()">ChartShow</button>


        <div id="chart"></div>

        
    @endsection


</html>

<script>
 {{-- Tensorflow --}}
 
function tensorFlowShow(){
    // 訓練數據
    const xVals = [0, 1, 2, 3, 4, 5,6,7,8,9];
    const yVals = [6, 12, 18, 24, 30, 36, 42, 48, 54, 60];

    // 標準化數據
    const xMax = Math.max(...xVals);
    const xMin = Math.min(...xVals);
    const yMax = Math.max(...yVals);
    const yMin = Math.min(...yVals);

    const normalizedXVals = xVals.map(x => (x - xMin) / (xMax - xMin));
    const normalizedYVals = yVals.map(y => (y - yMin) / (yMax - yMin));

    // 創建模型
    const model = tf.sequential();
    model.add(tf.layers.dense({ units: 1, inputShape: [1] }));
    model.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });

    // 訓練模型
    const xs = tf.tensor2d(normalizedXVals, [normalizedXVals.length, 1]);
    const ys = tf.tensor2d(normalizedYVals, [normalizedYVals.length, 1]);

    model.fit(xs, ys, { epochs: 500 }).then(() => {
    // 進行預測
        const testX = tf.tensor2d([(300 - xMin) / (xMax - xMin)], [1, 1]);
        const prediction = model.predict(testX);
        prediction.data().then(data => {
            const predictedY = data[0] * (yMax - yMin) + yMin;
            console.log(`預測命中高度: ${predictedY} 英尺`);

            // 可視化原始數據和模型預測數據 tfjs vis
            const values = [
            { x: xVals, y: yVals, label: '原始數據' },
            { x: xVals, y: model.predict(xs).dataSync().map(y => y * (yMax - yMin) + yMin), label: '模型預測結果' },
            { x: [300], y: [predictedY], label: '測試預測結果' }
            ];
            const surface = { name: 'mysurface', tab: 'tsjsreuslt' };
            tfvis.render.scatterplot(surface, { values, series: ['label'] }, { xLabel: '時間（小時）', yLabel: '高度（英尺）' });

            // 在網頁中顯示圖片
            const chartElement = document.getElementById('chart');
            //tfvis.renderTo(chartElement, surface);
          
        });
    });

}
function tensorFlowChartShow(){
    {{-- tfvis.show('mysurface'); --}}
    tfvis.visor.open('mysurface')
    console.log("show")


}

{{-- tfjs.vis --}}
function tensorFlowVis(){
 // 創建一些示例數據
    const data = Array.from({ length: 100 }, (_, i) => ({
    x: i,
    y: 2 * i + 1 + Math.random() * 10
    }));

    // 定義模型
    const model = tf.sequential();
    model.add(tf.layers.dense({ units: 1, inputShape: [1] }));

    // 編譯模型
    model.compile({
    loss: "meanSquaredError",
    optimizer: tf.train.sgd(0.1)
    });

    // 訓練模型
    const xs = data.map(d => d.x);
    const ys = data.map(d => d.y);
    const tensorXs = tf.tensor2d(xs, [xs.length, 1]);
    const tensorYs = tf.tensor2d(ys, [ys.length, 1]);
    async function train() {
    const history = await model.fit(tensorXs, tensorYs, {
        epochs: 50,
        callbacks: tfvis.show.fitCallbacks(
        { name: "Training Performance" },
        ["loss"],
        document.getElementById('chart')
        )
    });
    }
    train();
}

</script>