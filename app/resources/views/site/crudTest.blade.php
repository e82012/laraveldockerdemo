<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.js"></script>

@section('title', 'CRUD測試')

@extends('layouts.navbar')

{{-- modal component --}}
@component('components.comModal',[
    'id' => 'comModal_edit',
    'title' => '修改', 
    ])
    <form class="ui form" >
    @csrf
        <div class="field">
            <label>門市編號</label>
            <input type="text" name="storecode" id="form_edit_storecode" placeholder="門市編號" readonly>
        </div>
        <div class="field">
            <label>門市名稱</label>
            <input type="text" name="storename" id="form_edit_storename" placeholder="門市名稱">
        </div>
        <div class="field">
            <label>門市型態</label>
            <select name="storetype" id="form_edit_storetype">
                @foreach ($storeTypes as $type)
                    <option value="{{ $type }}">{{ $type }}</option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <label>售票機種類</label>
            <select name="opt2type" id="form_edit_opt2type">
                @foreach ($opt2Types as $type)
                    <option value="{{ $type }}">{{ $type }}</option>
                @endforeach
            </select>
        </div>
        <button class="ui mini inverted primary button" type="button" onclick = "updateStoreData()">確定修改</button>
        <button class="ui mini inverted red button" type="button" onclick = "deleteStoreData()">刪除資料</button>

    </form>

@endcomponent



@section('content')

    <p>CRUD測試</p>
    
    <form method="POST" action = "{{ route('createStore') }}">
        @csrf
        {{-- POST方式 --}}
        <div class="ui container ">
            <div class="ui compact segment">
                <div class="content">
                    {{-- Result Alert --}}
                    @if (Session::has('resultMsg'))
                        <div class="ui {{ Session::get('resultMsg')[0]}} tiny floating message">
                            {{ Session::get('resultMsg')[1]}} 
                        </div>
                    @endif
                    {{-- 新增 --}}
                    <div class="ui styled accordion">
                        <div class="title">
                            <i class="plus square outline icon"></i>
                            新增資料
                        </div>
                        <div class="content">
                            <div class="ui mini form transition hidden">
                                <div class="field">
                                    <label>門市編號</label>
                                    <input type="text" name="storecode" id="form_storecode" placeholder="門市編號">
                                    @error('storecode')
                                        <div class="ui pointing red basic label">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="field">
                                    <label>門市名稱</label>
                                    <input type="text" name="storename" id="form_storename" placeholder="門市名稱">
                                    @error('storename')
                                        <div class="ui pointing red basic label">
                                            {{$message}}
                                        </div>
                                    @enderror
                                </div>
                                <div class="field">
                                    <label>門市型態</label>
                                    <select name="storetype" id="form_storetype">
                                        @foreach ($storeTypes as $type)
                                            <option value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="field">
                                    <label>售票機種類</label>
                                    <select name="opt2type" id="form_opt2type">
                                        @foreach ($opt2Types as $type)
                                            <option value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="two fields">
                                    <button class="mini ui inverted blue button" name = "addNewStoreUsePost" type="submit" value="addPost" onclick ="checkinput('form')">確定新增(Post)</button>
                                    <button class="mini ui inverted orange button" name="addNewStoreUseAjax" type="button" onclick ="addAjax('form')" >確定新增(Ajax)</button>
                                </div>

                            </div>
                        </form>
                        </div>
                    </div>
                    {{-- 表單內容 --}}
                    <table class="ui table">
                        <thead>
                        <tr>
                            <th>門市編號</th>
                            <th>門市名稱</th>
                            <th>門市型態</th>
                            <th>售票機種類</th>
                            <th>修改</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($storeList as $s)
                                <tr>
                                    <td id="edtd_storecode_{{$s->id}}">{{$s->storecode}}</td>
                                    <td id="edtd_storename_{{$s->id}}">{{$s->storename}}</td>
                                    <td id="edtd_stype_{{$s->id}}">{{$s->stype}}</td>
                                    <td id="edtd_opt2_{{$s->id}}">{{$s->opt2}}</td>
                                    <td>  <button class="ui mini inverted red button" type="button" id = 'edit_{{$s->id}}' onclick = "modalshow('comModal_edit','{{$s->id}}')" value = "{{$s->id}}">修改</button> </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                        {{-- {{ $storeList->links() }}
                        <!-- 顯示分頁連結 --> --}}
                        <div class="ui pagination menu">
                            @if ($storeList->currentPage() > 1)
                                <a href="{{ $storeList->previousPageUrl() }}" class="item">上一頁</a>
                            @endif
                        
                            @for ($i = 1; $i <= $storeList->lastPage(); $i++)
                                @if ($i == 1 || $i == $storeList->lastPage() || ($i >= $storeList->currentPage() - 2 && $i <= $storeList->currentPage() + 2))
                                    <a href="{{ $storeList->url($i) }}" class="item{{ $storeList->currentPage() == $i ? ' active' : '' }}">{{ $i }}</a>
                                @elseif ($i == $storeList->currentPage() - 3 || $i == $storeList->currentPage() + 3)
                                    <span class="item">...</span>
                                @endif
                            @endfor
                        
                            @if ($storeList->hasMorePages())
                                <a href="{{ $storeList->nextPageUrl() }}" class="item">下一頁</a>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </form>

@endsection

<script>
$(document).ready(function() {

   window.onload = function(){
       $('.ui.accordion').accordion();

   };
});

/**
* 檢查欄位
* 依據type進行
* 1.from
* 新增資料用
* 2.modal
* 修改資料用
*/
function checkinput(n){
    const type = n;
    const chkSCode = document.getElementById(type +'_storecode').value;
    const chkSName = document.getElementById(type +'_storename').value;
    const chkSType = document.getElementById(type +'_storetype').value;
    const chkSOpt2 = document.getElementById(type +'_opt2type').value;
    {{-- console.log(chkSCode + "_" +chkSName +"_"+ chkSType +"_" + chkSOpt2) --}}

    //如果其中有一個空值，跳出alert並且停止送出
    if(chkSCode == '' || chkSName == '' || chkSType == '' || chkSOpt2 == '')
    {
        Swal.fire(
            '注意',
            '新增欄位中有資料未填寫',
            'warning'
        )
        //停止動作
        event.preventDefault();
    }
    else
    {
        var inputValue = {
            storecode:chkSCode,
            storename:chkSName,
            storetype:chkSType,
            opt2type:chkSOpt2
        }
        return inputValue
    }
}

function addAjax(n){
    // 先出現loading擋住
    Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
    // 進行欄位檢查
    const inputValue = checkinput(n);
    //執行ajax寫入
    console.log("insert")
    console.log(inputValue)
    $.ajax({
        type: 'POST',
        dataType:'JSON',
        url: '/site/createStore',
        data: {
            _token: '{{ csrf_token() }}',
            storecode:inputValue.storecode,
            storename:inputValue.storename,
            storetype:inputValue.storetype,
            opt2type:inputValue.opt2type,
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
        console.log("成功")
        console.log(response)
        // alert通知
        Swal.fire(
            '成功',
            response[1],
            'success'
        )
       
        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '資料新增失敗',
                'warning'
            )

        }
    });

}

// 修改門市資料
function updateStoreData(){
    console.log("update")
    // 先出現loading擋住
    Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
    // 抓到modal上的資料
    let storecode = document.getElementById('form_edit_storecode').value
    let storename = document.getElementById('form_edit_storename').value
    let storetype = document.getElementById('form_edit_storetype').value
    let opt2type = document.getElementById('form_edit_opt2type').value
    // 執行ajax
    $.ajax({
        type: 'POST',
        dataType:'JSON',
        url: '/site/updateStore',
        data: {
            _token: '{{ csrf_token() }}',
            storecode:storecode,
            storename:storename,
            storetype:storetype,
            opt2type:opt2type,
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
        console.log("成功")
        console.log(response)
        // alert通知
        Swal.fire(
            '成功',
            response[1],
            'success'
        )
        // 更新成功重新整理
        location.reload()
       
        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '資料更新失敗',
                'warning'
            )

        }
    })
}
// 刪除門市資料
function deleteStoreData(){
    console.log("delete")
    // 抓到modal上的資料
    let storecode = document.getElementById('form_edit_storecode').value
    let storename = document.getElementById('form_edit_storename').value
    // 提示是否確認刪除
    Swal.fire({
            // '是否刪除以下門市資料',
            // '門市編號 : '+ storecode + '<br>門市名稱 : ' + storename ,
            // 'warning'
            title: '是否刪除以下門市資料',
            text: '門市編號 : '+ storecode + ' 門市名稱 : ' + storename,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: '刪除',
            cancelButtonText: '取消',
            reverseButtons: true
        }).then((r)=>{
            // 按確定執行
            if(r.isConfirmed){
                // 先跳出loading
                Swal.fire({
                    title: '請稍後',
                    didOpen: () => {
                        Swal.showLoading()
                    },
                })
                // 執行ajax刪除
                $.ajax({
                    type: 'POST',
                    dataType:'JSON',
                    url: '/site/deleteStore',
                    data: {
                        _token: '{{ csrf_token() }}',
                        storecode:storecode,
                        // 請求的資料
                    },
                    success: function(response) {
                        // 處理回應
                    console.log("成功")
                    console.log(response)
                    // alert通知
                    Swal.fire(
                        '成功',
                        response[1],
                        'success'
                    )
                    // 更新成功重新整理
                    location.reload()
                
                    },
                    error: function(err){
                        console.log("失敗")
                        console.log(err)
                        Swal.fire(
                            '注意',
                            '資料更新失敗',
                            'warning'
                        )

                    }
                })
            }
        })

}



</script>
