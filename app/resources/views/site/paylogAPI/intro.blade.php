<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.js"></script>

{{-- <script src="{{ mix('/js/app.js') }}"></script> --}}


@section('title', 'PaylogAPI')
@extends('layouts.navbar')


@section('content')

    <p>PaylogAPI</p>

    <form method="POST" >
        @csrf
        {{-- Auth --}}
        @auth
            Auth
        @endAuth
        {{-- Guest --}}
        @guest
            Guest
        @endguest
        {{-- 內容 --}}
        <div class="ui container">
            <div class="ui segment">
                <div class="ui accordion">
                {{-- #1 index intro --}}
                <div class="active title">
                    <h4>
                        paylogIndex
                        <a class="ui basic label">intro</a>
                    </h4>
                </div>
                <div class=" active content">
                    <a class="ui label">
                        <i class="linkify icon"></i>
                        {{$url.'/paylog/intro'}}
                    </a>
                    <a class="ui label">
                        <i class="database icon"></i>
                        tbs_pay_log
                    </a>
                </div>
                <div class="ui divider"></div>

                {{-- 2 show all--}}
                <div class="title">
                    <h4>
                        Index_show_all
                        <a class="ui teal label">GET</a>
                    </h4>
                </div>
                <div class="content">
                    <a class="ui label">
                        <i class="linkify icon"></i>
                        {{$url.'/api/paylog/'}}
                    </a>
                    <div class="ui horizontal divider">
                        Usage
                    </div>
                    <button class="ui button" type="button" name = "show_All" id ="show_All" onclick="showAll()">Show all</button>
                </div>
                <div class="ui divider"></div>

                {{-- 3 show by id--}}
                <div class="title">
                    <h4>
                        Show_by_id
                        <a class="ui teal label">GET</a>
                    </h4>
                </div>
                <div class="content">
                    <a class="ui label">
                        <i class="linkify icon"></i>
                        {{$url.'/api/paylog/{ { id } }'}}
                    </a>
                    <div class="ui horizontal divider">
                        Usage
                    </div>
                    <form class = "ui form">
                    <div class="field">
                        <div class="ui action input">
                            <input type="text" name="show_id" id = "show_id" placeholder="ID">
                            <button class="ui button" type = "button" onclick="showById()">查詢</button>
                        </div>
                    </div>
                    </form>

                </div>
                <div class="ui divider"></div>

                {{-- 4 store--}}
                <div class="title">
                    <h4>
                        Store
                        <a class="ui pink label">POST</a>
                    </h4>
                </div>
                <div class="content">
                    <a class="ui label">
                        <i class="linkify icon"></i>
                        {{$url.'/api/paylog/'}}
                    </a>
                    <div class="ui horizontal divider">
                        Usage
                    </div>
                    <form class="ui form">
                        <div class="field">
                            <label>storecode</label>
                            <input type="text" name="store_storecode" id = "store_storecode" placeholder="storecode">
                        </div>
                        <div class="field">
                            <label>service</label>
                            <input type="text" name="store_service" id = "store_service" placeholder="service">
                        </div>
                        <div class="field">
                            <label>payment</label>
                            <input type="text" name="store_payment" id = "store_payment" placeholder="payment">
                        </div>
                        <div class="field">
                            <label>price</label>
                            <input type="text" name="store_price" id = "store_price" placeholder="price">
                        </div>
                        <button class="ui button" type="button" onclick="storeData()">Store</button>
                    </form>
                </div>
                <div class="ui divider"></div>

                {{-- 5 update--}}
                <div class="title">
                    <h4>
                        Update
                        <a class="ui orange label">PUT</a>
                    </h4>
                </div>
                <div class="content">
                    <a class="ui label">
                        <i class="linkify icon"></i>
                        {{$url.'/api/paylog/{ { id } }'}}
                    </a>
                    <div class="ui horizontal divider">
                        Usage
                    </div>
                    <form class="ui form">
                        <div class="field">
                            <div class="ui action input">
                              <input type="text" name="update_id" id = "update_id" placeholder="ID">
                              <button class="ui button" type="button" onclick="checkInputData()">查詢</button>
                            </div>
                          </div>
                        <div class="field">
                            <label>storecode</label>
                            <input type="text" name="update_storecode" id ="update_storecode" placeholder="storecode">
                        </div>
                        <div class="field">
                            <label>service</label>
                            <input type="text" name="update_service" id ="update_service" placeholder="service">
                        </div>
                        <div class="field">
                            <label>payment</label>
                            <input type="text" name="update_payment" id ="update_payment" placeholder="payment">
                        </div>
                        <div class="field">
                            <label>price</label>
                            <input type="text" name="update_price" id = "update_price" placeholder="price">
                        </div>
                        <div class="field">
                            <label>Opt</label>
                            <input type="text" name="update_opt" id ="update_opt" placeholder="opt">
                        </div>
                        <button class="ui button" type="button" onclick="updateData()">Update</button>
                    </form>
                </div>
                <div class="ui divider"></div>

                {{-- 6 destory--}}
                <div class="title">
                    <h4>
                        Destory
                        <a class="ui grey label">DELETE</a>
                    </h4>
                </div>
                <div class="content">
                    <a class="ui label">
                        <i class="linkify icon"></i>
                        {{$url.'/api/paylog/{ { id } }'}}
                    </a>
                    <div class="ui horizontal divider">
                        Usage
                    </div>
                    <div class="field">
                        <div class="ui action input">
                            <input type="text" name="destory_id" id ="destory_id" placeholder="ID">
                            <button class="ui button" type = button onclick="checkDeleteData()">刪除</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </form>
@endsection



<script>
$(document).ready(function() {

    window.onload = function(){
        $('.ui.accordion').accordion();
    };
});
// API function

// show All
function showAll(){
    console.log("showAll")
    // 先出現loading擋住
    Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
    // ajax
    $.ajax({
        type: 'GET',
        dataType:'JSON',
        url: '/api/paylog/',
        data: {
            _token: '{{ csrf_token() }}',
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
            console.log("成功")
            console.log(response)
            const aryLength = response.length
            // alert通知
            Swal.fire(
                '成功',
                '資料 : ' + aryLength,
                'success'
            )
        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '失敗',
                'warning'
            )

        }
    });

}
// show by id
function showById(){
    console.log("showById")
    const showId  = document.getElementById('show_id').value
    console.log(showId)
      // 先出現loading擋住
        Swal.fire({
            title: '請稍後',
            didOpen: () => {
                Swal.showLoading()
            },
        })
        // ajax
        $.ajax({
            type: 'GET',
            dataType:'JSON',
            url: '/api/paylog/'+showId,
            data: {
                _token: '{{ csrf_token() }}',
                // 請求的資料
            },
            success: function(response) {
                // 處理回應
                console.log("成功")
                console.log(response)
                // alert通知
                Swal.fire(
                    '成功',
                    'console',
                    'success'
                )
            },
            error: function(err){
                console.log("失敗")
                console.log(err)
                Swal.fire(
                    '注意',
                    '失敗',
                    'warning'
                )

            }
        });
    
}

// store data
function storeData(){
    console.log("store")
    // 取得資料
    const storecode = document.getElementById('store_storecode').value
    const service = document.getElementById("store_service").value
    const payment = document.getElementById("store_payment").value
    const price = document.getElementById("store_price").value
    console.log([storecode,service,payment,price])
   // 先出現loading擋住
    Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
    // ajax
    $.ajax({
        type: 'POST',
        dataType:'JSON',
        url: '/api/paylog/',
        data: {
            _token: '{{ csrf_token() }}',
            storecode:storecode,
            service:service,
            payment:payment,
            price:price,
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
            console.log("成功")
            console.log(response)
            // alert通知
            Swal.fire(
                '成功',
                'console',
                'success'
            )
            // 清空欄位
            document.getElementById('store_storecode').value = ''
            document.getElementById("store_service").value = ''
            document.getElementById("store_payment").value =''
            document.getElementById("store_price").value =''

        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '失敗',
                'warning'
            )

        }
    });
}

// update 1 : check input data
function checkInputData(){
    console.log("check input data")
    const updateId = document.getElementById("update_id").value
     // 先出現loading擋住
     Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
    // ajax
    $.ajax({
        type: 'GET',
        dataType:'JSON',
        url: '/api/paylog/'+updateId,
        data: {
            _token: '{{ csrf_token() }}',
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
            console.log("成功")
            console.log(response)
            // alert通知
            Swal.fire(
                '成功',
                'console',
                'success'
            )
            // 資料填入欄位
            document.getElementById('update_storecode').value = response['storecode']
            document.getElementById("update_service").value = response['service']
            document.getElementById("update_payment").value =response['payment']
            document.getElementById("update_price").value =response['price']
            document.getElementById("update_opt").value =response['opt1']
        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '失敗',
                'warning'
            )

        }
    });
}

//update 2 : update data
function updateData(){
    console.log("update")
    // 先出現loading擋住
    Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
    // 取得資料
        const updateId = document.getElementById('update_id').value
        const storecode = document.getElementById('update_storecode').value
        const service = document.getElementById("update_service").value
        const payment = document.getElementById("update_payment").value
        const price = document.getElementById("update_price").value
        const opt1 = document.getElementById("update_opt").value
    // ajax
    $.ajax({
        type: 'PUT',
        dataType:'JSON',
        url: '/api/paylog/'+updateId,
        data: {
            _token: '{{ csrf_token() }}',
            storecode:storecode,
            service:service,
            payment:payment,
            price:price,
            opt1:opt1
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
            console.log("成功")
            console.log(response)
            // alert通知
            Swal.fire(
                '成功',
                'console',
                'success'
            )
            // 欄位清空
            document.getElementById('update_id').value = ''
            document.getElementById('update_storecode').value = ''
            document.getElementById("update_service").value = ''
            document.getElementById("update_payment").value = ''
            document.getElementById("update_price").value = ''
            document.getElementById("update_opt").value = ''
        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '失敗',
                'warning'
            )

        }
    });

}

// DELETE : check && delete
function checkDeleteData(){
    console.log("check input data")
    const destoryId = document.getElementById("destory_id").value
    console.log(destoryId)
     // 先出現loading擋住
     Swal.fire({
        title: '請稍後',
        didOpen: () => {
            Swal.showLoading()
        },
    })
     // ajax
     $.ajax({
        type: 'GET',
        dataType:'JSON',
        url: '/api/paylog/'+destoryId,
        data: {
            _token: '{{ csrf_token() }}',
            // 請求的資料
        },
        success: function(response) {
            // 處理回應
            console.log("成功")
            console.log(response)
            // alert通知
            Swal.fire({
                title: '確定刪除以下資料嗎?',
                html:"<div class='ui message'>" + 
                    "storecode : " + response['storecode'] + "<br>" +
                    "service : " + response['service'] + "<br>" +
                    "payment : " + response['payment'] + "<br>" + 
                    "ctime : " + response['ctime']  + 
                    "</div>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '確認刪除'
                }).then((result) => {
                if (result.isConfirmed) {
                    // 先出現loading擋住
                    Swal.fire({
                        title: '請稍後',
                        didOpen: () => {
                            Swal.showLoading()
                        },
                    })
                    // 確認刪除 AJAX destory
                       // ajax
                        $.ajax({
                            type: 'DELETE',
                            dataType:'JSON',
                            url: '/api/paylog/'+destoryId,
                            data: {
                                _token: '{{ csrf_token() }}',
                                // 請求的資料
                            },
                            success: function(response) {
                                // 處理回應
                                console.log("成功")
                                console.log(response)
                                // alert通知
                                Swal.fire(
                                    '刪除成功',
                                    'console',
                                    'success'
                                )
                            },
                            error: function(err){
                                console.log("失敗")
                                console.log(err)
                                Swal.fire(
                                    '注意',
                                    '刪除失敗',
                                    'warning'
                                )

                            }
                        });
                    }
                })
        },
        error: function(err){
            console.log("失敗")
            console.log(err)
            Swal.fire(
                '注意',
                '查詢失敗',
                'warning'
            )

        }
    });

}


</script>
