<!DOCTYPE html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @section('title', 'index')
    @extends('layouts.navbar')
    @section('content')
        <h2 class="ui header">Welcome</h2>
        <div class="ui floating message">
            <h4>Version INFO</h4>
            <div class="ui horizontal list">
                <div class="item">
                    <img class="ui mini circular image" src="{{ asset('img/laravelIcon.png') }}">
                    <div class="content">
                    <div class="ui sub header">Laravel</div>
                    {{ $laravelVersion }}
                    </div>
                </div>
                <div class="item">
                    <img class="ui mini circular image" src="{{ asset('img/phpicon.png') }}">
                    <div class="content">
                    <div class="ui sub header">PHP</div>
                     {{ $phpVersion }}
                    </div>
                </div>
                <div class="item">
                    <img class="ui mini circular image" src="{{ asset('img/mysqlicon.png') }}" >
                    <div class="content">
                    <div class="ui sub header">MySql</div>
                    {{ $mySqlVersion[0]->version }}
                    </div>
                </div>
            </div>
        </div>


    @endsection


</html>
<script>
$.ajax({
    type: 'POST',
    dataType:'JSON',
    url: '{{ route('ajaxWeather') }}',
    data: {
        _token: '{{ csrf_token() }}',
        // 請求的資料
    },
    success: function(response) {
        // 處理回應
        {{-- console.log(response) --}}
        const Toast = Swal.mixin({
            toast: true,
            position: 'bottom-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
        Toast.fire({
        icon: 'success',
        title: response
        })
    },
    error: function(err){
        console.log("失敗")
        console.log(err)

    }
});
</script>