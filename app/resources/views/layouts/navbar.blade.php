{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"> --}}
{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>


{{-- sweetalert2 --}}
<script src="{{ asset('js/sweetalert2@11.js') }}"></script>
{{-- Semantic UI --}}
<script src="{{ asset('js/Semantic/semantic.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('js/Semantic/semantic.min.css') }}" />
{{--  --}}


 {{-- 登入用modalContent  塞入 components.comModal.slot --}}
@section('modalContent')
  <form class="ui form" method="POST" action="{{ route('login') }}">
    @csrf
    <div class="field">
      <label for="email">{{ __('Email Address') }}</label>
      <input
        id="email"
        type="email"
        class="@error('email') error @enderror"
        name="email"
        value="{{ old('email') }}"
        required
        autocomplete="email"
        autofocus
      />
      @error('email')
        <div class="ui pointing red basic label">
          {{ $message }}
        </div>
      @enderror
    </div>

    <div class="field">
      <label for="password">{{ __('Password') }}</label>
      <input
        id="password"
        type="password"
        class="@error('password') error @enderror"
        name="password"
        required
        autocomplete="current-password"
      />
      @error('password')
        <div class="ui pointing red basic label">
          {{ $message }}
        </div>
      @enderror
    </div>

    <div class="field">
      <div class="ui checkbox">
        <input
          type="checkbox"
          name="remember"
          id="remember"
          {{ old('remember') ? 'checked' : '' }}
        />
        <label for="remember">{{ __('Remember Me') }}</label>
      </div>
    </div>

    <button type="submit" class="ui primary button">
      {{ __('Login') }}
    </button>
    <button
      type="button"
      class="ui secondary button"
      onclick="location.href='{{ route('register') }}'"
    >
      {{ __('Register') }}
    </button>

    @if (Route::has('password.request'))
      <br>
      <a class="section" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
      </a>
    @endif
  </form>
@endsection
{{-- slot 參數--}}
@include('components.comModal', [
    'id' => 'comModal_login',
    'title' => '登入',
    'footer' => '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">取消</button>',
])


<style>

</style>

<head>
  <title>@yield('title') - Laravel2023</title>
  <!-- 其他 head 資訊 -->
</head>
{{-- MENU --}}
<div class="ui menu">
{{--  靠左按鈕 --}}
  <a class="logo" href="/">
    <img src="{{ asset('img/ame.gif') }}" width="30" >
  </a>
  <a class="item" href="/site/about">
    關於
  </a>
  <div class="ui pointing dropdown link item">
    <span class="text">選單</span>
      <i class="dropdown icon"></i>
    <div class="menu">
      <div class="header">測試</div>
      <a class="item" href="/site/crudTest" >CRUD</a>
      <a class="item" href="/paylog/intro" >API測試</a>

      <div class="item">
        <i class="dropdown icon"></i>
        <span class="text">更多...</span>
        <div class="menu">
          <div class="header">Type1</div>
          <div class="item">A</div>
          <div class="item">B</div>
          <div class="item">C</div>
          <div class="item">D</div>
          <div class="divider"></div>
          <div class="header">Type2</div>
          <div class="item">E</div>
          <div class="item">F</div>
          <div class="item">G</div>
      </div>
      </div>
      {{-- 分隔 --}}
      <div class="divider"></div>
      <div class="header">其他</div>
      <a class="item">Status</a>
    </div>
  </div>
{{--  靠右按鈕 --}}
{{-- 未登入 --}}
  @guest
      @if (Route::has('login'))
            <a class="item right floated" onclick="modalshow('comModal_login')">
              登入
            </a>
      @endif
{{-- 已登入 --}}
  @else
  <div class="right menu">
    <div class="ui pointing dropdown item">
      {{ Auth::user()->name }} <i class="dropdown icon"></i>
      <div class="menu">
        <a class="item">修改資料</a>
        <a class="item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('登出') }}
        </a>
      </div>
    </div>
  </div>
{{--  --}}
  <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
      @csrf
  </form>
  @endguest
</div>

{{-- 定義content位置 --}}
<div class="container">
    @yield('content')
</div>

{{-- bottom footer --}}
<div class="ui mini inverted three item bottom fixed menu">
  <div class="item"></div>
  <div class="item">© {{date("Y")}} By Ou</div>
  <div class="item">
      <div id = "gitInfo" class="ui right floating dropdown item">
        Git Commit
        <div class="menu">
        </div>
      </div>
  </div>
</div>




<script>
// ui dropdown
$('.ui.dropdown')
  .dropdown()
;
// modal Show
function modalshow(n,edid = null)
{
  const modalId = '#'+ n
  $(modalId)
    .modal({
      // blurring: true
    })
    .modal('show')
    // 將edid加入
    if(edid != null)
    {
      // 先抓到table的資料
      let eStorecode = document.getElementById('edtd_storecode_' + edid).innerText
      let eStorename = document.getElementById('edtd_storename_' + edid).innerText
      let eStoretype = document.getElementById('edtd_stype_' + edid).innerText
      let eStoreopt2 = document.getElementById('edtd_opt2_' + edid).innerText

      // 放入modal
      document.getElementById('form_edit_storecode').value = eStorecode
      document.getElementById('form_edit_storename').value = eStorename
      document.getElementById('form_edit_storetype').value = eStoretype
      document.getElementById('form_edit_opt2type').value = eStoreopt2
    }
}

//AJAX GIT COMMIT 
$.ajax({
    type: 'POST',
    dataType:'JSON',
    url: '{{ route('ajaxGitcommit') }}',
    data: {
        _token: '{{ csrf_token() }}',
        // 請求的資料
    },
    success: function(response) {
        // 處理回應
       {{-- console.log(response) --}}
       $('#gitInfo')
        .dropdown({
            values: [
              {
                name     : response[0][0] +"<br><br>"+ "<a class='ui pointing basic label'>"+response[1][0]+"</a>",
                {{-- value    : 'female', --}}
                selected : true
              }
            ]
          })
    },
    error: function(err){
        console.log("失敗")
        console.log(err)

    }
});
</script>