{{-- 測試 --}}
<div class="ui mini modal" id = "{{$id}}">
  <div class="header">
    {{$title}} 
  </div>
  <div class="image content">
    @switch($id)
        @case('comModal_login')
            @yield('modalContent')
            @break
        @case('modal_editSlot')
            @yield('modal_editSlot')
            @break
        @default
            @if(isset($slot))
              {!! $slot !!}
            @endif
            @break
      @endswitch

  </div>
</div>


        


{{-- 
    使用方式
    @include('components.comModal', [
    'id' => 'deleteModal',
    'title' => '確認刪除',
    'slot'=>'asdasda',
    'footer' => 
    '<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">取消</button><button type="button" class="btn btn-danger">確認刪除</button>'
    ])

 --}}

