<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\siteController;
use App\Http\Controllers\PaylogController;
use Illuminate\Foundation\Application;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| Route::any
| Route::post
| Route::get
| Route::view
*/

// index
Route::any('/', [App\Http\Controllers\siteController::class, 'index']);

/**
 * GROUP
 */
// 1. For Site
Route::prefix('site')->group(function () {
    // Route::match(['get','post'],'/about', 'App\Http\Controllers\siteController@about');
    Route::match(['get','post'],'/about', [App\Http\Controllers\siteController::class, 'about']);
    Route::match(['get','post'],'/testpage', [App\Http\Controllers\siteController::class, 'testpage']);
    // CRUD測試
    Route::match(['get','post'],'/crudTest', [App\Http\Controllers\siteController::class, 'crudTest']);
    // CRUD測試 - Create
    Route::match(['get','post'],'/createStore', [App\Http\Controllers\siteController::class, 'createStore'])->name('createStore');
    // Route::post('/createStore', [App\Http\Controllers\siteController::class, 'createStore'])->name('createStore');
    // CRUD測試 - Update
    Route::match(['get','post'],'/updateStore', [App\Http\Controllers\siteController::class, 'updateStore'])->name('updateStore');
    // CRUD測試 - Delete
    Route::match(['get','post'],'/deleteStore', [App\Http\Controllers\siteController::class, 'deleteStore'])->name('deleteStore');

});

/**
 * Dropdown page 先放這邊
 */
Route::prefix('dropdown')->group(function () {
    // Route::match(['get','post'],'/about', 'App\Http\Controllers\siteController@about');
    // ______ViewPage檔名____________________________Controller________________controller function名稱
    Route::any('/dropdown_1', [App\Http\Controllers\dropdownController::class, 'dropdown1Page']);

});

// ajax----------------------------------
// // AJAX Weather
// Route::post('/ajax/weather', [App\Http\Controllers\SiteController::class, 'ajaxWeather'])->name('ajax.weather');
// // GIT commit 
// Route::post('/ajax/gitcommit', [App\Http\Controllers\SiteController::class, 'ajaxGitcommit'])->name('ajax.gitcommit');

// 
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/**
 * 
 */
Route::prefix('paylog')->group(function(){
    Route::any('/intro', [PaylogController::class, 'paylogIndex']); // intro page
});