<?php

use App\Http\Controllers\PaylogController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\siteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
PayLog API (RESTful API)
測試基本CRUD ，web有一個簡單應用介紹
*/
Route::prefix('paylog')->group(function(){
    Route::get('/', [PaylogController::class, 'index']); // Get all articles
    Route::get('/{id}', [PaylogController::class, 'show']); // Get a specific article
    Route::post('/', [PaylogController::class, 'store']); // Create a new article
    Route::put('/{id}', [PaylogController::class, 'update']); // Update an article
    Route::delete('/{id}', [PaylogController::class, 'destroy']); // Delete an article
});
/**
 * 
 */
// AJAX Weather
// Route::post('/ajax/weather', [SiteController::class, 'ajaxWeather'])->name('ajax.weather');
Route::post('/site/ajaxWeather', [SiteController::class, 'ajaxWeather'])->name('ajaxWeather'); 

// GIT commit 
// Route::post('/ajax/gitcommit', [SiteController::class, 'ajaxGitcommit'])->name('ajax.gitcommit');
Route::post('/site/ajaxGitcommit', [SiteController::class, 'ajaxGitcommit'])->name('ajaxGitcommit');; 


//--- 
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
