<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// 使用model
use App\Models\Tbs_pay_log;
// query錯誤訊息
use Illuminate\Database\QueryException;
use Symfony\Component\HttpFoundation\Response;
// 驗證
use Illuminate\Validation\ValidationException;

// 
class PaylogController extends Controller
{
    // IntroPage
    public function paylogIndex(){
        $ipaddress = request()->ip();
        // dump($ipaddress);
        $paylog = '123';
        $url = url('/');
        // 建立API URL清單
    


        return view('site/paylogAPI/intro',compact('paylog','url'));

    }
    //API index
    public function index(){
        $paylog = Tbs_pay_log::all();
        return response()->json($paylog);
    }

    // show by id
    public function show($id){
        $paylog = Tbs_pay_log::findOrFail($id);
        return response()->json($paylog);
    }

    // Store
    public function store(Request $request){
        // 設定
        $now = date("Y-m-d H:m:s");
        // 寫入資料時填入
        $request->merge([
                'ctime'=> $now,
                'ip'=>$request->ip(),
                'opt1'=>'1',
        ]);
        // 驗證資料欄位
        $this->validatePaylog($request);
        // 建立資料，將$request轉為陣列形式
        $paylog = Tbs_pay_log::create($request->all());
        // Status 201 >> create成功 
        return response()->json($paylog,201);

    }

    //Update
    public function update(Request $request,$id){
        // 設定
        $now = date("Y-m-d H:m:s");
        // 寫入資料時填入
        $request->merge([
                'ctime'=> $now,
                'ip'=>$request->ip(),
        ]);
        $this->validatePaylog($request);

        $paylog = Tbs_pay_log::findOrFail($id);
        $paylog->update($request->all());

        // status 200 請求成功
        return response()->json($paylog,200);
    } 

    // Delete
    public function destroy($id){
        $paylog=Tbs_pay_log::findOrFail($id);
        $paylog->delete();

        // status 204 >> 無內容
        return response()->json(null,204);
    }

    // store validator
    public function validatePaylog(Request $request){

        return $this->validate($request,[
            'storecode'=>'required|size:6',
            'service'=>'required',
            'payment'=>'required',
            'price'=>'required',
            'ip'=>'required',
            'ctime'=>'required',
            'opt1'=>'required',
        ]);
    }


}
