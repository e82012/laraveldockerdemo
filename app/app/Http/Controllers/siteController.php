<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
// DB Facade 執行原生的 SQL 查詢
use Illuminate\Support\Facades\DB;
// 權限
use Illuminate\Support\Facades\Auth;
// 驗證
use Illuminate\Support\Facades\Validator;

// 使用model
use App\Models\Tbs_area;
use App\Models\Tbs_store;
use Illuminate\Auth\Events\Validated;

// query錯誤訊息
use Illuminate\Database\QueryException;
// query分頁用
use Illuminate\Pagination\Paginator;

class siteController extends Controller
{
    public function __construct()
    {
        // 需要登入才能使用
        // $this->middleware('auth');
    }
    // index
    public function index()
    {

        $phpVersion = phpversion();
        $laravelVersion = App::version();
        $mySqlVersion = DB::select('SELECT VERSION() as version');

        // DB連接 與 sql select 方式
            // 使用預設DB連接
            // $qryData_1 = DB::select('SELECT * FROM tbp_perform_emp_QRticket');
            // dump($qryData_1);
        
            // 指定DB連接
            // $qryData_2 = DB::connection('testdrive')->table('tbp_perform_emp_QRticket')->get();
            // dump($qryData_2);

        // model
            // 查找
                // SELECT全部
                $tbs_area = Tbs_area::all();
                // SELECT areaCode為3310
                $tbs_area_select_qryall = Tbs_area::where('areaCode', '3310')->get();
                // dump($tbs_area_select_qryall);
                // SELECT 第一筆 areaCode為3310
                $tbs_area_select_first = Tbs_area::where('areaCode', '3310')->first();
                // dump($tbs_area_select_first);
                // 使用like方式 
                $tbs_area_select_like = Tbs_area::where('areaCode','LIKE','%3310%')->get();
                // dump($tbs_area_select_like);
                // AND 用法
                $tbs_area_select_AND = Tbs_area::where('areaCode','3310')->where('areaname','北一區')->get();
                // dump($tbs_area_select_AND);
                // OR 用法
                $tbs_area_select_OR = Tbs_area::where('areaCode','3310')->orWhere('areaname','北一區')->get();
                // dump($tbs_area_select_OR);

            // 寫入DB
                // new model
                $tbsAreaModel = new Tbs_area();
                // 資料放入欄位(指向欄位名稱)
                $tbsAreaModel->areaCode = '1234';
                $tbsAreaModel->areaname = '新區域';
                // 寫入
                // $tbsAreaModel->save();
        // 測試



  
        return view('welcome',compact('phpVersion','laravelVersion','mySqlVersion'));
        // 只render view
        // return view('welcome');
        // return資料回view 使用compact再把回傳的變數名稱放進去
        // return view('welcome', compact('phpVersion','laravelVersion'));
    }
    
    // about view
    public function about()
    {
        $postValue = isset($_POST['postValue'])?$_POST['postValue']:'';
        // dump($postValue);
        // 取得需要傳到視圖的資料
        $data = [
            'title' => 'About Us',
            'description' => 'This is the about us page.'
        ];
        $dataBB = 'sdada';
        // dump($data);
        
        // DB model測試
        // $areaAry = Area::all();
        // dump($areaAry);
        // foreach($areaAry as $a)
        // {
        //     echo $a['areaname'];
        // }

        // 按了postValue
        if(isset($_POST['postValue']))
        {
            dump($_POST);
        }


        // 傳送資料到about視圖並回傳該視圖--------------
        // return view('site/about', [
        //     'data' => $data,
        //     'dataBB'=>$dataBB,
        //     'postValue'=>$postValue
        // ]);
        return view('site/about', compact('data', 'dataBB', 'postValue'));
    }

    // weather Ajax
    public function ajaxWeather()
	{
		$city = isset($_POST['city'])?$_POST['city']:'5';
        $weatherMsg = '';
        if($city != ''){
            $nowTime = date("A");
            switch($nowTime)
            {
                case "AM":
                    $nowStatus = 0;
                    break;
                case "PM":
                    $nowStatus = 1;
                    break;
            }
            $weatherURL = "https://opendata.cwb.gov.tw/fileapi/v1/opendataapi/F-C0032-001?Authorization=rdec-key-123-45678-011121314&format=JSON";
            $weatherJson = json_decode(file_get_contents($weatherURL));
            $location = $weatherJson->cwbopendata->dataset->location;
            // 資料整理
            $kao = $location[$city]; //城市
            $cityName = $kao->locationName;
            $weatherStatus = $kao->weatherElement[0]->time[$nowStatus]->parameter->parameterName;
            $tempMax = $kao->weatherElement[1]->time[$nowStatus]->parameter->parameterName;
            $tempMin = $kao->weatherElement[2]->time[$nowStatus]->parameter->parameterName;
            $tempFeel = $kao->weatherElement[3]->time[$nowStatus]->parameter->parameterName;
            $rainPOP = $kao->weatherElement[4]->time[$nowStatus]->parameter->parameterName;

            $weatherMsg =$cityName.
            "\n"."氣候狀態:".$weatherStatus.
            "\n"."溫度區間:".$tempMin."°C~".$tempMax."°C".
            "\n"."體感狀態:".$tempFeel;

            // return $weatherMsg;
            // print json_encode($weatherMsg);
        }
        else
            $weatherMsg = 'Error!No localKey';


        return response()->json($weatherMsg);


	}
    // Git commit Ajax
    public function ajaxGitcommit()
    {
        // get Git Commit
        $gitUrl = "https://gitlab.com/e82012/laraveldockerdemo/-/commits/master?feed_token=glft-0f5911007f3f6f92da52b0c48d9a2d01c848b67e9b3e2035aac282c6cde35d62-7147432&format=atom";
        $gitJson = simplexml_load_file($gitUrl);
        $gitCommitData = $gitJson->entry;
        $gitCommitTitle = $gitCommitData->title[0];
        $gitCommitUpdated = $gitCommitData->updated[0];
        $gitCommitAry = array();
        array_push($gitCommitAry,$gitCommitTitle,$gitCommitUpdated);

       return response()->json($gitCommitAry);


    }
    
    // CRUD測試
    public function crudTest(Request $request)
    {
        // 門市種類
            $storeTypes = $this->getStoreTypes();
        // 售票機種類
            $opt2Types = $this->getOpt2Type();
        // 資料載入
            // $storeList = Tbs_store::where('opt1','1')->get();
            $storeList = Tbs_store::where('opt1','1')->paginate(10);

            // dump($storeList);

        return view('site/crudTest',compact('storeTypes','opt2Types','storeList'));

        // return view('site/crudTest');
    }
    /**
     * CRUD Create功能
     * 1.POST方式，返回固定頁面
     * 2.AJAX return 結果
     */
    public function createStore(Request $request)
    {
        // 結果訊息
        $resultMsg = array();

        // 先檢查使用者，再進行新增
        if (Auth::check()) {
            $user = Auth::user();
            $username = $user->name;
            $storecode = $request->input('storecode',null);
            $storename = $request->input('storename',null);
            $storetype = $request->input('storetype',null);
            $opt2type = $request->input('opt2type',null);
            // 檢查
            $validator = Validator::make($request->all(),
            [
                'storecode'=>'required|max:6',
                'storename'=>'required',
                'storetype'=>'required',
                'opt2type'=>'required',
            ]);
            // 驗證檢查
            if($validator->fails())
            {
                array_push($resultMsg,"red","新增失敗_".$validator->messages());
                echo $resultMsg[1]; 
                return redirect()->back()->withErrors($validator)->withInput()
                    ->with([
                        'resultMsg'=>$resultMsg
                    ]);
            }
            else{                        
                // 進行新增
                $tbsStoreModel = new Tbs_store();
                $tbsStoreModel->storecode = $storecode;
                $tbsStoreModel->storename = $storename;
                $tbsStoreModel->stype = $storetype;
                $tbsStoreModel->opt1 = '1';
                $tbsStoreModel->opt2 = $opt2type;
                $tbsStoreModel->ctime = now();
                $tbsStoreModel->cemp = $username;
                try{
                    $tbsStoreModel->save();
                    array_push($resultMsg,"green","新增成功");
                } catch(QueryException $e)
                {
                    array_push($resultMsg,"red","新增失敗_".$e->getMessage()); 
                }
        
            }
        } else {
            array_push($resultMsg,"red","尚未登入，請先登入再進行操作");
        }


        // 1.如果是從crudTest的addNewStoreUsePost送出表單，則返回site/crudTest，並且回傳結果
        if(isset($request->addNewStoreUsePost))
        {
            echo $resultMsg[1];
            return redirect('site/crudTest')->with([
                'resultMsg'=> $resultMsg,
            ]);
        }
        // 2.Ajax新增方式，回傳結果
        else
        {
            return response()->json($resultMsg);
        }
        // return view('site/crudTest');


    }
    /**
     * CRUD Update功能
     * ajax
     */
    public function updateStore(Request $request)
    {
        $resultMsg = array();
        if (Auth::check()) {
            $user = Auth::user();
            $username = $user->name;
            // Query Builder寫法
            // $tbsStoreModel = Tbs_store::find($request->storecode);
            try{
                Tbs_store::where('storecode',$request->storecode)
                ->update([
                    'storename'=>$request->storename,
                    'stype'=>$request->storetype,
                    'opt2'=>$request->opt2type,
                    'utime'=>now(),
                    'uemp'=>$username,
                ]);
                array_push($resultMsg,"green","修改成功");
            } catch(QueryException $e) {
                array_push($resultMsg,"red","更新失敗",$e->getMessage());
            }
        }
        else
            array_push($resultMsg,"red","尚未登入，請先登入再進行操作");

        // 返回資料
        return response()->json($resultMsg);

    }
    /**
     * CRUD Delete功能
     * ajax
     */
    public function deleteStore(Request $request)
    {
        $resultMsg = array();
        if (Auth::check()) {
            $user = Auth::user();
            $username = $user->name;
            // Query Builder寫法
            try{
                // 做軟刪除方式，將opt1改為0
                Tbs_store::where('storecode',$request->storecode)
                ->update([
                    'opt1'=>'0',
                    'utime'=>now(),
                    'uemp'=>$username,
                ]);
                array_push($resultMsg,"green","刪除成功");
            } catch(QueryException $e) {
                array_push($resultMsg,"red","更新失敗",$e->getMessage());
            }
        }
        else
            array_push($resultMsg,"red","尚未登入，請先登入再進行操作");

        // 返回資料
        return response()->json($resultMsg);
    }

    // 門市種類
    public function getStoreTypes()
    {
        $storeTypes = array(
            '一般',
            '染護',
            '精剪',
            'pk100'
        );

        return $storeTypes;
    }
    //售票機種類
    public function getOpt2Type()
    {
        $opt2Types = array(
            '1','2','3','4','5'
        );

        return $opt2Types;
    } 


}
