<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    // table名稱
    protected $table = 'area';
    // key
    protected $primaryKey = 'id';
    // 欄位
    protected $fillable = ['id','area','areaname','empid','empname','memo','opt1','opt2','opt3','cemp','ctime','uemp','utime','ip'];
}

