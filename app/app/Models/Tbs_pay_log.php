<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tbs_pay_log extends Model
{
    use HasFactory;

    protected $table = 'tbs_pay_log';
    public $timestamps = false;
    public $primaryKey = 'id';
    // fillable
    protected $fillable = [
        'storecode',
        'service',
        'payment',
        'price',
        'ip',
        'ctime',
        'opt1',
        'opt2',
    ];
}
