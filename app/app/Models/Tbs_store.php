<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tbs_store extends Model
{
    use HasFactory;

    protected $table = 'tbs_store';
    public $timestamps = false;
    public $primaryKey = 'id';

}
